	       R E V I S E D   S P E C I F I C A T I O N

					  Due 2:00 AM, Friday, 03 November 2017

CPSC 323   Homework #4   An Ounce of Compression

REMINDERS:  Do not under any circumstances copy another student's code or give
a copy of your code to another student.  After discussing the assignment with
another student (such discussions should be noted in your log file), do not
take any written or electronic record away and engage in a full hour of mind-
numbing activity before you work on it again.  Sharing with another student ANY
written or electronic document related to the course (e.g., code or test cases)
is a violation of this policy.

Since code reuse is an important part of programming, you may incorporate
published code (e.g., from textbooks or the Net) in your programs, provided
that you give proper attribution in your source and in your statement of major
difficulties AND THAT THE BULK OF THE CODE SUBMITTED IS YOUR OWN.


(60 points) Write file compression and decompression filters:

      encode [-m MAXBITS] [-p]

      decode

encode reads a stream of characters from the standard input, compresses it
using the Lempel-Ziv-Welch algorithm, and writes the stream of codes to the
standard output as a stream of bits packed into 8-bit bytes.  decode reads
from the standard input a byte stream written by encode, decompresses the
stream of codes, and writes the stream of characters to the standard output.

encode writes codes using the smallest number of bits required to specify valid
codes at the time (e.g., 9 bits when there are 512 valid codes, but 10 bits
once the next code is assigned), up to a maximum of 12 bits (or MAXBITS if the
-m flag is specified).  In effect, this limit determines the maximum number of
codes that can be assigned.

When all available codes have been assigned, the default is to stop assigning
codes.  With the prune option -p, encode instead creates a new string table
containing the one-character strings and all other strings that are prefixes of
some *other* string in the original table.  Note that the codes for some strings
may change in order to be able to use the smallest possible code length. 

encode and decode are a single C program (i.e., they are hard links to the
same inode) whose behavior is determined by the name under which it is invoked
(i.e., the 0-th command-line argument).  The -m and -p options may appear in
any order and any number of times, with the last occurrence of each option
superseding any earlier ones.

Use the submit command to turn in your log file and the source files for
encode/decode (including a Makefile) as Homework #4.

YOU MUST SUBMIT YOUR FILES (INCLUDING THE LOG FILE) AT THE END OF ANY SESSION
WHERE YOU HAVE SPENT AT LEAST ONE HOUR WRITING OR DEBUGGING CODE, AND AT LEAST
ONCE EVERY HOUR DURING LONGER SESSIONS.  (All submissions are retained.)

UP TO *10 POINTS* MAY BE DEDUCTED FOR SUBMITTING WITH INSUFFICIENT FREQUENCY.


Notes
~~~~~
1. encode and decode write a one-line message to stderr and exit when an
   invalid option is specified.  decode also writes an error message if it
   detects a file that encode could not have generated.  However, encode need
   not detect the case where MAXBITS is not a positive integer representable as
   an int.

2. If MAXBITS <= 8 (= CHAR_BIT in <limits.h>) or MAXBITS > 20 (= 2*CHAR_BIT+4),
   then encode replaces MAXBITS by 12 (= CHAR_BIT+4).

3. The -m and -p options are omitted for decode since this is more convenient
   for the user.  (What would happen if you forgot the value of MAXBITS for a
   particular compressed file?) Thus this information must be represented in
   the output from encode.  For example, to represent MAXBITS:

   a. The first byte could contain the maximum code size.

   b. A special code (e.g., 0 or 111...111) could be used to signal that the
      code size should be increased (which has the added benefit of keeping
      encode and decode synchronized).

   Or you could use both.  This flexibility in the compressed form must be
   documented in your program.

4. The set of strings assigned codes consists of (CODE,PREF,CHAR) triples.
   Decode must find PREF and CHAR given the CODE for a nonempty string, which
   is done most easily using an array of (PREF,CHAR) structs indexed by CODE.
   On the other hand, encode must find CODE given the pair (PREF,CHAR).  Since
   linear search in an array of structs would be prohibitively expensive, it
   uses a faster search algorithm:  double hashing with a hash table of
   SIZE = 2^MAXBITS (= 2 to the power MAXBITS); see

     https://en.wikipedia.org/wiki/Double_hashing

   for details.  One possible set of hash functions for a pair (p,c) of
   prefix p and char c is

     #define INIT(p,c) (p)                              // Initial slot
     #define STEP(p,c) (2 * (((p)<<CHAR_BIT)|(c)) + 1)  // Step to next slot
     #define REDUCE(h) ((h) & ((SIZE)-1))               // Reduce to [0,SIZE-1)

   where REDUCE(h) maps h to a value between 0 and SIZE-1.  Then the
   sequence of slots visited for prefix p and char c is:

     INIT(p,c), REDUCE(INIT(p,c)+STEP(p,c)), REDUCE(INIT(p,c)+2*STEP(p,c)),
       REDUCE(INIT(p,c)+3*STEP(p,c)), ..., REDUCE(INIT(p,c)+(SIZE-1)*STEP(p,c))

   Note:  To make search and insertion reasonably efficient, encode and decode
   limit the maximum number of triples stored in the hash table (and thus the
   maximum number of codes that are assigned) to .99*SIZE.

6. encode handles both text and binary files (i.e., stdin may contain any of
   the 256 possible byte values, including the null character '\0').

7. encode and decode should be relatively bombproof.  However, they may assume
   that malloc() and realloc() will never fail.

8. encode and decode need not free() all storage before exiting, but all heap
   storage (i.e., any blocks allocated by malloc(), calloc(), or realloc())
   must be reachable at that time.

9. Do NOT use the pow() function; use the shift operator << instead.

A. Your solution should incorporate the following files (see also Hint #1):

     Hwk4/code.h    Header file defining putBits(), flushBits(), and getBits(),
		     functions that write/read a stream of bits packed in bytes

     Hwk4/code.c    Source file for putBits(), flushBits(), and getBits()

     Hwk4/code.o    Object file for putBits(), flushBits(), and getBits()

   Moreover, your Makefile and sources must reference the originals; e.g.,

     #include "/c/cs323/Hwk4/code.h"

   rather than

     #include "code.h"

   When the environment variable STAGE exists and is equal to 1, Hwk4/code.o
   writes/reads codes using the format "%d\n".  When STAGE is equal to 2, it
   writes/reads the number of bits and the code using "%d:%d\n".  You may
   find this useful when debugging.  (See Hwk4/code.h for details.)

   Note:  Hwk4/encode and Hwk4/decode can be directed to use the same
   representations of the code sequence, as in the bash command

     % STAGE=2 /c/cs323/Hwk4/encode < ... .

   Moreover, if the environment variable DBG is set, they dump the table
   to the file DBG.encode or DBG.decode.

B. The assignment is called lzw, so the test script will be invoked as

     % /c/cs323/Hwk4/test.lzw

   or as

     % /c/cs323/bin/testit 4 lzw

C. The degree of LZW compression (that is, the length of the output from
   encode) depends on the file, the command-line arguments and how they are
   represented in the output of encode), and the number of special codes
   (e.g., EMPTY, INCR_NBITS, and PRUNE).  Indeed, the compressed file will
   be larger than the original in some cases.  Thus all tests of size will
   be in comparison with that given by Hwk4/encode (which the scripts
   assume is a correct implementation of LZW) and will be relatively loose.

D. The following capabilities will be worth _at_most_ the number of points
   shown below (these values include tests requiring multiple functionalities):

   * (20 points) increasing the code size from 9 to MAXBITS bits (vs. always
     using MAXBITS bits)

   * (12 points) handling binary files

   * (12 points) implementing the -p option

   * (12 points) keeping the amount of storage associated with the string table
     (reasonably) low:  4 points for keeping the number of bytes BELOW 8*SIZE,
     4 points for BELOW 12*SIZE, and 4 points for BELOW 16*SIZE.  (Recall that
     SIZE = 2^MAXBITS is the number of slots in the hash table.)
     
     Hints:  What are bit fields? How many times does a value need to
     be stored?  What must be stored during pruning?

     Points to Ponder:  If you embedded this algorithm in a device, you would
     want to use as little memory as possible to reduce the cost.  Can you do
     so with only 6*SIZE bytes?  What if you limit MAXBITS <= 16?

     Normally the standard input and the standard output in a C program are
     buffered.  The Standard I/O Library calls malloc() to allocate the
     buffers (whose length is 4096 or 8192 bytes) when each stream is first
     accessed (e.g., by a call to getchar(), scanf(), putchar(), or
     printf()).

     The tests for a small footprint count _all_ storage allocated,
     including that for buffers.  This could push your program over the
     limit imposed by one or more of these tests.

     To turn off buffering (and thereby the allocation of buffers), insert
     the following lines at the beginning of main():

     setvbuf (stdin,  NULL, _IONBF, 0);    // Avoid malloc()ing buffers
     setvbuf (stdout, NULL, _IONBF, 0);    //   for stdin/stdout

   You may find it easier to implement encode/decode initially without the above
   capabilities and then add them later.


Hints on Encode/Decode
~~~~~~~~~~~~~~~~~~~~~~
1. It is MUCH easier to write the core of encode/decode in three stages:

   Stage 1:  The number of bits required to represent all codes is ignored.
     encode outputs codes as ASCII integers, one per line; and decode inputs
     codes in the same format.  (That is, run with STAGE=1; see Hwk4/code.h.)

   Stage 2:  As more strings are assigned codes, the number of bits sent and
     received grows from 9 to MAXBITS.  encode outputs codes in the form
     NBITS:CODE, where NBITS and CODE are ASCII integers (e.g., 10:666); and
     decode inputs codes in the same format, verifying that the number of bits
     expected agrees with NBITS.  (That is, run with STAGE=2.)

   Stage 3: encode outputs the codes as a stream of bits, packed into chars,
     and decode inputs the codes in the same format.

   Since the "compressed" data is human-readable in Stages 1 and 2, code is
   easier to debug.  Moreover, the first part of the test script will not check
   the size of the output from encode so that the code from these stages should
   be able to pass all such tests.

   When implementing new functionality (e.g., the -p flag), revert to STAGE=1
   even if the rest of your code is at a higher stage.

   You could also start with Stage 0, where MAXBITS is fixed at 9 and linear
   search (which is not prohibitively expensive for at most 512 items) replaces
   double hashing.

2. While the pseudo-code for decode uses a stack to output the string of
   characters corresponding to a given code, it may be easier to use a
   recursive function instead.

3. Use int's rather than char's to hold character values, at least initially.
   The increase in storage is minor, but experience suggests that the savings
   in debugging time can be major.

4. Since compression followed by decompression should reproduce the original,
   the acid test for encode/decode is that

	% encode < FILE | decode | cmp - FILE

   does not produce output for any file FILE.  In this command

   * encode reads bytes from FILE and compresses them into a sequence of codes
     that are written to its standard output;

   * decode reads this sequence of codes and decompresses them into a sequence
     of bytes that are written to its standard output; and

   * cmp reads this sequence of bytes and compares it to the contents of FILE,
     printing a message only if it finds a discrepancy (shorter, longer, or
     at least one character different).

   "od -bc" and "cat -v -e -t" are other useful commands for looking at files.

5. It may be helpful to write scaffolding code to:

   * Dump to a file the contents of the string table, with each line containing
     a code (if one was assigned), the corresponding (PREF,CHAR) pair, and the
     string to which it corresponds (written as a sequence of integers, not
     chars, so that they are visible).  You can use this function to compare
     the tables produced by encode and decode, or (by omitting the code, PREF,
     and CHAR and then sorting) to compare which strings are in the table.

   * Verify that the table is consistent; e.g., in each (PREF, CHAR) pair, CHAR
     is a valid character and PREF is a valid code (or some special value).

   * After inserting something in the hash table, search() for it immediately
     (i.e., from within the insert() function) to verify that it can be found
     where you inserted it.

   * Monitor the average search time (= the ratio of the total number of hash
     cells visited within the search() function divided by the number of calls)
     and verify that it never gets too large.  Hint:  Use assert() or a
     conditional fprintf(stderr,...) so as not to affect the standard output.

6. The primary difference between text files and binary files is that the
   latter usually contain NUL characters (i.e., '\0').  Thus if your code works
   on text files but not on binary files, you know where to start looking.

7. What to do if LZW works in Stage 2 but fails in Stage 3:

   * If the output from decode is too short but is otherwise correct, check
     whether decode receives the last code from encode (i.e., whether encode
     calls flushBits() before exiting).

   * If the output from decode is completely garbled, check whether decode
     receives the first code from encode correctly.

8. What to do if the output from decode is garbled for some input to encode:

   * Dump the string tables from encode and from decode in a human-readable
     format.  Are they the same, except possibly for the effects of the last
     code?  If not, shorten the file (using "head -c N") until the tables would
     be the same if one more character were removed; and trace the flow of
     execution in both encode and decode to see where one or both go wrong.

9. The LZW handout gives two ways to handle the KwKwK problem:

   * Patching:  Assume that the encode clock starts at 0 and ticks every time
     that encode writes a code; and the decode clock starts at 0 and ticks
     every time that decode reads a code.  Then when a given string is added to
     the table the encode and decode clocks show the time.  The only difference
     is that encode knows both PREF and CHAR, but decode knows only PREF and
     must later patch the CHAR field.  This makes it slightly easier to
     synchronize.
      
     Delaying:  By having encode insert each string one tick later so that
     decode knows both PREF and CHAR, you avoid the KwKwK problem entirely,
     except that you must avoid inserting strings more than once.  For example,
     encode sends code(Kw) which leads to adding KwK; and then sends code(Kw)
     again (since KwK is not yet in the table), which could lead to adding KwK
     again.

   Another is to have both encode and decode delay the insertion.

								CS-323-10/17/17
								
